package service;

import exceptions.InvalidIdException;
import persistence.BranchDao;
import persistence.CarDao;
import persistence.model.Branch;
import persistence.model.Car;

import java.util.List;

public class BranchService {
    private BranchDao branchDao;
    private CarDao carDao;
    private CarService carService;

    public BranchService(BranchDao branchDao, CarDao carDao, CarService carService) {
        this.branchDao = branchDao;
        this.carDao = carDao;
        this.carService = carService;
    }

    public void addBranch(Branch branch) {
        branchDao.save(branch);
    }

    public List<Branch> viewBranches() {
        return branchDao.findAll();
    }

    public Branch findById(int id) {
        return branchDao.findById(id);
    }

    public List<Branch> findBranchesByCity(int id) {
        return branchDao.findBranchesByCity(id);
    }

    public void addCarToBranch(int carId, int branchId) {
        Branch branchFound = branchDao.findById(branchId);
        Car carFound = carDao.findById(carId);
        carFound.setBranch(branchFound);
        carDao.update(carFound);
    }

    public void deleteBranch(int id) throws InvalidIdException {
        Branch choosenBranch = branchDao.findById(id);
        if (choosenBranch == null) {
            throw new InvalidIdException();
        } else {
            choosenBranch.setCity(null);
            branchDao.update(choosenBranch);
            for (Car car : carService.findCarsByBranch(id)) {
                car.setBranch(null);
                carDao.update(car);
            }
            branchDao.delete(choosenBranch);
        }
    }
}
