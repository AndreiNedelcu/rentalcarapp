package service;

import exceptions.InvalidIdException;
import persistence.BranchDao;
import persistence.CarDao;
import persistence.CityDao;
import persistence.model.Branch;
import persistence.model.Car;
import persistence.model.City;

import java.util.List;

public class CityService {

    private CityDao cityDao;
    private BranchDao branchDao;
    private CarDao carDao;

    public CityService(CityDao cityDao, BranchDao branchDao, CarDao carDao) {
        this.cityDao = cityDao;
        this.branchDao = branchDao;
        this.carDao = carDao;
    }

    public void addCity(City city) {
        cityDao.save(city);
    }

    public List<City> viewCities() {
        return cityDao.findAll();
    }

    public City findById(int id) {
        return cityDao.findById(id);
    }

    public void deleteCity(int cityId) throws InvalidIdException {
        City city = cityDao.findById(cityId);
        if (city == null) {
            throw new InvalidIdException();
        } else {
            for (Branch branch : branchDao.findBranchesByCity(cityId)) {
                for (Car car : carDao.findCarsByBranch(branch.getId())) {
                    car.setBranch(null);
                    carDao.update(car);
                }
                branch.setCity(null);
                branchDao.update(branch);
                branchDao.delete(branch);
            }
            cityDao.delete(city);
        }
    }

    public void addBranchToCity(int branchId, int cityId) {
        Branch branch = branchDao.findById(branchId);
        City city = cityDao.findById(cityId);
        branch.setCity(city);
        branchDao.update(branch);

    }

}
