package service;

import exceptions.InvalidIdException;
import persistence.BranchDao;
import persistence.CarBookingDao;
import persistence.CarDao;
import persistence.model.Car;
import persistence.model.CarBooking;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CarService {
    private CarDao carDao;
    private BranchDao branchDao;
    private CarBookingDao carBookingDao;

    public CarService(CarDao carDao, BranchDao branchDao, CarBookingDao carBookingDao) {
        this.carDao = carDao;
        this.branchDao = branchDao;
        this.carBookingDao = carBookingDao;
    }

    public void addCar(Car car) {
        carDao.save(car);
    }

    public List<Car> viewCars() {
        return carDao.findAll();
    }

    public Car findByID(int id) {
        return carDao.findById(id);
    }

    public List<Car> findCarsByBranch(int id) {
        return carDao.findCarsByBranch(id);
    }

    public void editCarPrice(int id, double newPrice) {
        Car car = carDao.findById(id);
        car.setPricePerDay(newPrice);
        carDao.update(car);
    }

    public void deleteCar(int id) throws InvalidIdException {
        Car car = carDao.findById(id);
        if (car == null) {
            throw new InvalidIdException();
        } else {
            car.setBranch(null);
            carDao.update(car);
            carDao.delete(car);
        }
    }

    public List<Car> getAvailableCars(int branchId, String startDate, String endDate) throws ParseException {

        List<Car> carsInBranch = carDao.findCarsByBranch(branchId);

        Date dateConvStart = new SimpleDateFormat("dd/MM/yyyy").parse(startDate);

        Date dateConvEnd = new SimpleDateFormat("dd/MM/yyyy").parse(endDate);

        List<Car> availableCars = new ArrayList<Car>();

        for (Car car : carsInBranch) {
            List<CarBooking> carBookings = carBookingDao.findCarBookingsByCarId(car.getId());
            boolean canBeBooked = true;
            for (CarBooking carBooking : carBookings) {
                boolean canNotBeBooked = dateConvEnd.after(carBooking.getStartDate()) && dateConvStart.before(carBooking.getEndDate());
//                        (carBooking.getStartDate().before(dateConvStart) &&
//                        dateConvStart.after(carBooking.getEndDate())) ||
//                        (carBooking.getStartDate().before(dateConvEnd) &&
//                                dateConvEnd.after(carBooking.getEndDate()));
                if (canNotBeBooked) {
                    // Masina nu poate fi rezevata
                    canBeBooked = false;
                }
            }
            if (canBeBooked) {
                availableCars.add(car);
            }
        }
        return availableCars;
    }

    public void saveCarBooking(Car car, String startDate, String endDate) throws ParseException {
        CarBooking carBooking = new CarBooking();
        carBooking.setCar(car);
        Date dateConvStart = new SimpleDateFormat("dd/MM/yyyy").parse(startDate);
        carBooking.setStartDate(dateConvStart);
        Date dateConvEnd = new SimpleDateFormat("dd/MM/yyyy").parse(endDate);
        carBooking.setEndDate(dateConvEnd);
        carBookingDao.save(carBooking);
    }


}
