package exceptions;

public class InvalidIdException extends Exception{

    public InvalidIdException() {
        super("Invalid ID, please try again!");
    }
}
