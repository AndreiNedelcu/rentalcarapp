package persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import persistence.model.Branch;
import persistence.model.Car;
import persistence.model.City;

import java.util.ArrayList;
import java.util.List;

public class CityDao {

    private SessionFactory sessionFactory;

    public CityDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(City city) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(city);
        transaction.commit();
        session.close();
    }

    public List<City> findAll() {
        Session session = sessionFactory.openSession();
        List<City> cities = session.createQuery("from City", City.class).list();
        session.close();
        return cities;
    }

    public City findById(int id) {
        Session session = sessionFactory.openSession();
        City city = session.find(City.class, id);
        session.close();
        return city;
    }

    public void update(City city) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(city);
        transaction.commit();
        session.close();
    }

    public void delete(City city) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(city);
        transaction.commit();
        session.close();
    }
}
