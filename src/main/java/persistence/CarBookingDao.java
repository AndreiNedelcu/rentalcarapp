package persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import persistence.model.Car;
import persistence.model.CarBooking;

import java.util.List;

public class CarBookingDao {

    private SessionFactory sessionFactory;

    public CarBookingDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(CarBooking carBooking) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(carBooking);
        transaction.commit();
        session.close();
    }

    @SuppressWarnings("unchecked")
    public List<CarBooking> findCarBookingsByCarId(int id) {
        Session session = sessionFactory.openSession();
        String hqlQueryString = "from CarBooking where car_id = :id";
        Query<CarBooking> query = session.createQuery(hqlQueryString);
        query.setParameter("id", id);
        List<CarBooking> carBookings = query.list();
        session.close();
        return carBookings;
    }

}
