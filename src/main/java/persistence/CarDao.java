package persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import persistence.model.Branch;
import persistence.model.Car;
import persistence.model.City;

import java.util.ArrayList;
import java.util.List;

public class CarDao {

    private SessionFactory sessionFactory;

    public CarDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Car car) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(car);
        transaction.commit();
        session.close();
    }

    public List<Car> findAll() {
        Session session = sessionFactory.openSession();
        List<Car> cars = session.createQuery("from Car", Car.class).getResultList();
        session.close();
        return cars;
    }

    @SuppressWarnings("unchecked")
    public List<Car> findCarsByBranch(int id) {
        Session session = sessionFactory.openSession();
        String hqlQueryString = "from Car where branch_id = :id";
        Query<Car> query = session.createQuery(hqlQueryString);
        query.setParameter("id", id);
        List<Car> cars = query.list();
        session.close();
        return cars;
    }

    public Car findById(int id) {
        Session session = sessionFactory.openSession();
        Car car = session.find(Car.class, id);
        session.close();
        return car;
    }

    public void update(Car car) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(car);
        transaction.commit();
        session.close();
    }

    public void delete(Car car) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(car);
        transaction.commit();
        session.close();
    }

}
