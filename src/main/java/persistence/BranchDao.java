package persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import persistence.model.Branch;
import persistence.model.Car;
import persistence.model.City;

import java.util.ArrayList;
import java.util.List;

public class BranchDao {

    private SessionFactory sessionFactory;

    public BranchDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Branch branch) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(branch);
        transaction.commit();
        session.close();
    }

    public List<Branch> findAll() {
        Session session = sessionFactory.openSession();
        List<Branch> branches = session.createQuery("from Branch", Branch.class).list();
        session.close();
        return branches;
    }

    public Branch findById(int id) {
        Session session = sessionFactory.openSession();
        Branch branch = session.find(Branch.class, id);
        session.close();
        return branch;
    }

    @SuppressWarnings("unchecked")
    public List<Branch> findBranchesByCity(int id) {
        Session session = sessionFactory.openSession();
        String hqlQueryString = "from Branch where city_id = :id";
        Query<Branch> query = session.createQuery(hqlQueryString);
        query.setParameter("id", id);
        List<Branch> branches = query.list();
        session.close();
        return branches;
    }

    public void update(Branch branch) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(branch);
        transaction.commit();
        session.close();
    }

    public void delete(Branch branch) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(branch);
        transaction.commit();
        session.close();
    }
}
