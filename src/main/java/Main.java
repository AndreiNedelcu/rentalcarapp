import org.hibernate.SessionFactory;
import persistence.BranchDao;
import persistence.CarBookingDao;
import persistence.CarDao;
import persistence.CityDao;
import persistence.model.CarBooking;
import service.BranchService;
import service.CarService;
import service.CityService;
import ui.*;

public class Main {
    public static void main(String[] args) {
        HibernateConfig hibernateConfig = new HibernateConfig();

        SessionFactory sessionFactory = hibernateConfig.getSessionFactory();

        CarDao carDao = new CarDao(sessionFactory);
        BranchDao branchDao = new BranchDao(sessionFactory);
        CityDao cityDao = new CityDao(sessionFactory);
        CarBookingDao carBookingDao = new CarBookingDao(sessionFactory);

        CarService carService = new CarService(carDao, branchDao, carBookingDao);
        BranchService branchService = new BranchService(branchDao, carDao, carService);
        CityService cityService = new CityService(cityDao, branchDao, carDao);

        CarUi carUi = new CarUi(carService, branchService);
        BranchUi branchUi = new BranchUi(branchService);
        CityUi cityUi = new CityUi(cityService);
        CarRentalUi carRentalUi = new CarRentalUi(cityService, branchService, carService);

        AppMenuUi appMenuUi = new AppMenuUi(carUi, branchUi, cityUi, carRentalUi);

        appMenuUi.startUi();
    }
}
