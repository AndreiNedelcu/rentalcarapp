package ui;

import persistence.model.Branch;
import persistence.model.Car;
import service.BranchService;
import service.CarService;
import exceptions.InvalidIdException;

import java.util.List;
import java.util.Scanner;

public class CarUi {

    private CarService carService;
    private BranchService branchService;
    private Scanner scanner;

    public CarUi(CarService carService, BranchService branchService) {
        this.carService = carService;
        this.branchService = branchService;
    }

    public void startCarUi() {
        scanner = new Scanner(System.in);
        int option;
        do {
            System.out.println(" ");
            System.out.println("1.Add car");
            System.out.println("2.View all cars");
            System.out.println("3.View cars by branch");
            System.out.println("4.Edit car price");
            System.out.println("5.Delete car");
            System.out.println("0.Exit");
            option = scanner.nextInt();
            scanner.nextLine();
            switch (option) {
                case 1:
                    addCarUi();
                    break;
                case 2:
                    viewCarsUi();
                    break;
                case 3:
                    viewCarsByBranchUi();
                    break;
                case 4:
                    editCarPriceUi();
                    break;
                case 5:
                    deleteCarUi();
                    break;
            }
        } while (option != 0);
    }

    private void addCarUi() {
        System.out.println("Enter the brand:");
        String brand = scanner.nextLine();
        System.out.println("Enter the model:");
        String model = scanner.nextLine();
        System.out.println("Enter the price per day:");
        double pricePerDay = scanner.nextDouble();
        scanner.nextLine();
        System.out.println("Enter the color:");
        String color = scanner.nextLine();
        Car car = new Car();
        car.setBrand(brand);
        car.setModel(model);
        car.setPricePerDay(pricePerDay);
        car.setColor(color);
        carService.addCar(car);
    }

    private void viewCarsUi() {
        List<Car> cars = carService.viewCars();
        for (Car car : cars) {
            System.out.println(car.getId() + ". " + car.getBrand() + " " + car.getModel()
                    + " <" + car.getColor() + "> " + ", price/day: " + car.getPricePerDay() + " ron.");
        }
    }

    private void viewCarsByBranchUi() {
        List<Branch> branches = branchService.viewBranches();
        for (Branch branch : branches) {
            System.out.println(branch.getId() + "." + branch.getName());
        }
        System.out.println("Please choose a branch: ");
        int branchChoose = scanner.nextInt();
        scanner.nextLine();
        for (Car car : carService.findCarsByBranch(branchChoose)) {
            System.out.println(car.getId() + "." + car.getBrand() + " " + car.getModel());
        }
    }

    private void editCarPriceUi() {
        viewCarsUi();
        System.out.println("Enter ID of car:");
        int carId = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Enter new price of car:");
        double newPrice = scanner.nextDouble();
        scanner.nextLine();
        carService.editCarPrice(carId, newPrice);
    }

    private void deleteCarUi() {
        viewCarsUi();
        System.out.println("Enter ID of car that will be deleted:");
        int carId = scanner.nextInt();
        scanner.nextLine();
        try {
            carService.deleteCar(carId);
        } catch (InvalidIdException iie) {
            System.out.println(iie.getMessage());
        }
    }
}
