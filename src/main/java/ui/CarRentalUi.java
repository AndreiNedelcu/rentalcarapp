package ui;

import persistence.model.Branch;
import persistence.model.Car;
import persistence.model.City;
import service.BranchService;
import service.CarService;
import service.CityService;

import java.text.ParseException;
import java.util.List;
import java.util.Scanner;

public class CarRentalUi {
    private CityService cityService;
    private BranchService branchService;
    private CarService carService;
    Scanner scanner;


    public CarRentalUi(CityService cityService, BranchService branchService, CarService carService) {
        this.cityService = cityService;
        this.branchService = branchService;
        this.carService = carService;
    }

    public void startCarRentalUi() {

        scanner = new Scanner(System.in);
        int option;
        do {
            System.out.println(" ");
            System.out.println("1.Rent Car");
            System.out.println("0.Exit");
            option = scanner.nextInt();
            scanner.nextLine();
            switch (option) {
                case 1:
                    rentCarUi();
                    break;
            }
        } while (option != 0);
    }

    private void rentCarUi() {
        List<City> cities = cityService.viewCities();
        for (City city : cities) {
            System.out.println(city.getId() + "." + city.getName());
        }
        System.out.println("Please choose a city: ");
        int choosenCity = scanner.nextInt();
        scanner.nextLine();


        List<Branch> branches = branchService.findBranchesByCity(choosenCity);
        for (Branch branch : branches) {
            System.out.println(branch.getId() + "." + branch.getName());
        }
        System.out.println("Please choose a branch: ");
        int choosenBranch = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Choose the start date: ");
        String startDate = scanner.nextLine();

        System.out.println("Choose the end date: ");
        String endDate = scanner.nextLine();

        try {
            for (Car car : carService.getAvailableCars(choosenBranch, startDate, endDate)) {
                System.out.println(car.getId() + "." + car.getBrand() + " " + car.getModel() + " <" + car.getPricePerDay() + " lei>");
            }
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("Please choose a car: ");
        int choosenCar = scanner.nextInt();
        scanner.nextLine();
        Car carChoosen = carService.findByID(choosenCar);
        System.out.println("You choose " + carChoosen.getModel() + " " + carChoosen.getBrand());
        System.out.println("Thank you for choose us!");
        try {
            carService.saveCarBooking(carChoosen, startDate, endDate);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }

    }
}
