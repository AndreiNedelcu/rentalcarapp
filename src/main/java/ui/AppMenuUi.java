package ui;

import persistence.model.Car;

import java.util.Scanner;

public class AppMenuUi {

    private CarUi carUi;
    private BranchUi branchUi;
    private CityUi cityUi;
    private CarRentalUi carRentalUi;

    public AppMenuUi(CarUi carUi, BranchUi branchUi, CityUi cityUi, CarRentalUi carRentalUi) {
        this.carUi = carUi;
        this.branchUi = branchUi;
        this.cityUi = cityUi;
        this.carRentalUi = carRentalUi;
    }

    public void startUi() {
        Scanner scanner = new Scanner(System.in);
        int option;
        do {
            System.out.println(" ");
            System.out.println("1.Car management");
            System.out.println("2.Branch management");
            System.out.println("3.City management");
            System.out.println("4.Rent car");
            System.out.println("0.Exit");
            option = scanner.nextInt();
            scanner.nextLine();
            switch (option) {
                case 1:
                    carUi.startCarUi();
                    break;
                case 2:
                    branchUi.startBranchUi();
                    break;
                case 3:
                    cityUi.startCityUi();
                    break;
                case 4:
                    carRentalUi.startCarRentalUi();
                    break;
            }
        } while (option != 0);
    }
}
