package ui;

import persistence.model.Branch;
import service.BranchService;
import exceptions.InvalidIdException;

import java.util.List;
import java.util.Scanner;

public class BranchUi {
    private BranchService branchService;

    public BranchUi(BranchService branchService) {
        this.branchService = branchService;
    }

    Scanner scanner;

    public void startBranchUi() {
        scanner = new Scanner(System.in);
        int option;
        do {
            System.out.println(" ");
            System.out.println("1.Add a new branch");
            System.out.println("2.View all branches");
            System.out.println("3.View branches by city");
            System.out.println("4.Add car to branch");
            System.out.println("5.Delete branch");
            System.out.println("0.Exit");
            option = scanner.nextInt();
            scanner.nextLine();
            switch (option) {
                case 1:
                    addBranchUi();
                    break;
                case 2:
                    viewBranchesUi();
                    break;
                case 3:
                    viewBranchesByCityUi();
                    break;
                case 4:
                    addCarToBranchUi();
                    break;
                case 5:
                    deleteBranchUi();
                    break;
            }
        } while (option != 0);
    }

    private void addBranchUi() {
        System.out.println("Enter name:");
        String name = scanner.nextLine();
        Branch branch = new Branch();
        branch.setName(name);
        branchService.addBranch(branch);
    }

    public void viewBranchesUi() {
        List<Branch> branches = branchService.viewBranches();
        for (Branch branch : branches) {
            System.out.println(branch.getId() + "." + branch.getName());
        }
    }

    private void viewBranchesByCityUi() {
        viewBranchesUi();
        System.out.println("Choose the city:");
        int cityChoose = scanner.nextInt();
        scanner.nextLine();
        for (Branch branch : branchService.findBranchesByCity(cityChoose)) {
            System.out.println(branch.getId() + "." + branch.getName());
        }
    }

    private void addCarToBranchUi() {
        System.out.println("Enter ID of Car: ");
        int carId = scanner.nextInt();
        System.out.println("Enter ID of Branch: ");
        int branchId = scanner.nextInt();
        branchService.addCarToBranch(carId, branchId);
    }

    private void deleteBranchUi() {
        viewBranchesUi();
        System.out.println("Enter ID of branch that will be deleted: ");
        int branchId = scanner.nextInt();
        scanner.nextLine();
        try {
            branchService.deleteBranch(branchId);
        } catch (InvalidIdException iie) {
            System.out.println(iie.getMessage());
        }
    }
}

