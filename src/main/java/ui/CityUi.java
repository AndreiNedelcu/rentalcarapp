package ui;

import persistence.model.City;
import service.CityService;
import exceptions.InvalidIdException;

import java.util.List;
import java.util.Scanner;

public class CityUi {

    private CityService cityService;

    public CityUi(CityService cityService) {
        this.cityService = cityService;
    }

    Scanner scanner;

    public void startCityUi() {
        scanner = new Scanner(System.in);
        int option;
        do {
            System.out.println(" ");
            System.out.println("1.Add city");
            System.out.println("2.View cities");
            System.out.println("3.Add branch to city");
            System.out.println("4.Delete city");
            System.out.println("0.Exit");
            option = scanner.nextInt();
            scanner.nextLine();
            switch (option) {
                case 1:
                    addCityUi();
                    break;
                case 2:
                    viewCitiesUi();
                    break;
                case 3:
                    addBranchToCityUi();
                    break;
                case 4:
                    deleteCityUi();
                    break;
            }
        } while (option != 0);
    }


    private void addCityUi() {
        System.out.println("Enter name:");
        String name = scanner.nextLine();
        City city = new City();
        city.setName(name);
        cityService.addCity(city);
    }

    private void viewCitiesUi() {
        List<City> cities = cityService.viewCities();
        for (City city : cities) {
            System.out.println(city.getId() + "." + city.getName());
        }
    }

    private void addBranchToCityUi() {
        System.out.println("Enter ID of Branch: ");
        int branchId = scanner.nextInt();
        System.out.println("Enter ID of City: ");
        int cityId = scanner.nextInt();
        cityService.addBranchToCity(branchId, cityId);
    }

    private void deleteCityUi() {
        viewCitiesUi();
        System.out.println("Enter ID of city that will be deleted:");
        int cityId = scanner.nextInt();
        scanner.nextLine();
        try {
            cityService.deleteCity(cityId);
        } catch (InvalidIdException iie) {
            System.out.println(iie.getMessage());
        }
    }
}
