import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import persistence.model.Branch;
import persistence.model.Car;
import persistence.model.CarBooking;
import persistence.model.City;

import java.util.Properties;

public class HibernateConfig {

    private SessionFactory sessionFactory;

    public HibernateConfig() {
        Configuration configuration = new Configuration();
        Properties properties = new Properties();
        properties.put(Environment.URL, "jdbc:mysql://localhost:3306/rentalcar");
        properties.put(Environment.USER, "test");
        properties.put(Environment.PASS, "test");
        properties.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
        properties.put(Environment.HBM2DDL_AUTO, "update");

        configuration.setProperties(properties);

        configuration.addAnnotatedClass(Car.class);
        configuration.addAnnotatedClass(Branch.class);
        configuration.addAnnotatedClass(City.class);
        configuration.addAnnotatedClass(CarBooking.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(properties).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }


    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}

